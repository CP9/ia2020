﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlgoritmosGeneticos
{
    public partial class Form1 : Form
    {
        /* Cromosomas

         0 => Altura
         1 => Color de la Fruta
         2 => Follaje

    */
        Arbol[]  arboles = new Arbol[6];

        Random rnd = new Random( unchecked((int)DateTime.Now.Ticks) );

        int generacion = 0;

        Arbol padre;
        Arbol madre;

        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < 6; i++)
            {
                Arbol arbol = new Arbol();

                arbol.Cromosomas.Add(new Cromosoma( rnd.Next(20, 50), rnd.Next(25, 45)));

                arbol.Cromosomas.Add(new Cromosoma(rnd.Next(0, 3), rnd.Next(0, 3)));

                arbol.Cromosomas.Add(new Cromosoma(rnd.Next(10, 40), rnd.Next( 15, 55)));

                arboles[i] = arbol;
            }

            grilla.DataSource = arboles;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void CalcularAdaptacion()
        {
            int altura, color, follaje;

            if (radioButton1.Checked)
            {
                altura = 0;
            }
            else if (radioButton2.Checked)
            {
                altura = 1;
            }
            else
            {
                altura = 2;
            }


            if (radioButton4.Checked)
            {
                color = 0;
            }
            else if (radioButton4.Checked)
            {
                color = 1;
            }
            else
            {
                color = 2;
            }

            if (radioButton5.Checked)
            {
                follaje= 0;
            }
            else if (radioButton6.Checked)
            {
                follaje = 1;
            }
            else
            {
                follaje = 2;
            }

            int alturaTmp = 0, colorTmp = 0, follajeTmp = 0;


            for (int n = 0; n < 6; n++)
            {
               switch (altura)
               {
                    case 0:
                        {
                            //rango 10 a 100
                            alturaTmp = arboles[n].Cromosomas[0].DevolverExpresion() / 100;
                            break;
                        }
                    case 1:
                        {
                            //rango 100 a 200
                            alturaTmp = arboles[n].Cromosomas[0].DevolverExpresion() / 100;
                            break;
                        }

                    case 2:
                        {

                            //rango 200 a 300
                            alturaTmp = arboles[n].Cromosomas[0].DevolverExpresion() / 100;
                            break;
                        }
                }


                colorTmp = arboles[n].Cromosomas[1].DevolverExpresion();
     

                switch (follaje)
                {
                    case 0:
                        {
                            //rango 10 a 50
                            follajeTmp = arboles[n].Cromosomas[2].DevolverExpresion() /50;
                            break;
                        }
                    case 1:
                        {
                            //rango 50 a 150
                            follajeTmp = arboles[n].Cromosomas[2].DevolverExpresion() / 150;
                            break;
                        }

                    case 2:
                        {

                            //rango 150 a 250
                            follajeTmp = arboles[n].Cromosomas[2].DevolverExpresion() / 250;
                            break;
                        }
                }

                if (follajeTmp > 1)
                {
                    follajeTmp = 1 / follajeTmp;
                }

                if (alturaTmp > 1)
                {
                    alturaTmp = 1 / alturaTmp;
                }
                arboles[n].Adaptacion = (alturaTmp + colorTmp + follajeTmp) / 3;
            }
           
        }

        public void SeleccionarPadres()
        {
            padre = arboles[0];

            for (int i = 1; i < 6; i++)
            {
                if (padre.Adaptacion < arboles[i].Adaptacion)
                {
                    padre = arboles[i];
                }
            }

            for (int i = 0; i < 6; i++)
            {
                if (madre == null || madre.Adaptacion < arboles[i].Adaptacion)
                {
                    if (arboles[i] != padre)
                    {
                        madre = arboles[i];
                    }
                }
            }
        }

        public void CrossOver()
        {
            Arbol arbol = new Arbol();

            for (int i = 0; i < 3; i++)
            {

                arbol.Cromosomas.Add( new Cromosoma( padre.Cromosomas[i].DevolverExpresion(), madre.Cromosomas[i].DevolverExpresion()     ) );

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CalcularAdaptacion();

            SeleccionarPadres();

            CrossOver();

        }
    }
}
