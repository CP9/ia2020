﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoritmosGeneticos
{
    class Arbol
    { 
        /* Cromosomas
         
             0 => Altura
             1 => Color de la Fruta
             2 => Follaje

       */


        private List<Cromosoma> cromosomas = new List<Cromosoma>() ;

        public List<Cromosoma> Cromosomas
        {
            get { return cromosomas ; }            
        }

        private double adaptacion;

        public double Adaptacion
        {
            get { return adaptacion; }
            set { adaptacion = value; }
        }




        public string Expresion
        {
            get { return this.Convertir(); }
     
        }


        public string Convertir()
        {

            string resultado = string.Empty;
            foreach (Cromosoma c in this.cromosomas)
            {

                resultado += "( " + c.X.ToString() + " , " + c.Y.ToString() + " => " + c.DevolverExpresion().ToString() + ")   ";
            }


            return resultado;

        }

    }
}
