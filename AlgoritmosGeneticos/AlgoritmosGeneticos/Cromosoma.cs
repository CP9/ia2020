﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoritmosGeneticos
{
    class Cromosoma
    {
        public static Random rnd = new Random();

        int expresion;

        internal Cromosoma()
        {
        }
        internal Cromosoma(int xX, int yY)
        {
            x = xX;
            y = yY;            

            expresion = (rnd.Next(0,2) == 0)? x: y;
  
        }


        public int DevolverExpresion()
        {

            return expresion;
            
        }

        private int x; //1 o 0

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }
    }
}
