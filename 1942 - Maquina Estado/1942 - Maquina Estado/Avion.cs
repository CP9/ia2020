﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _1942___Maquina_Estado
{
    public class Avion
    {
        private bool vivo = true;

        private bool powd = false;

        private bool powa = false;

        private bool aviones2 = false;

        private int vidas = 3;


        public Avion()
        {
            estado = Estado.normal;
        }

        private Estado estado;
        public Estado Estado
        {
            get { return estado; }
            set
            {
                estado = value;
            }
        }

        public void Chocar()
        {
            vivo = false;
            powd = false;
            powa = false;
            aviones2 = false;
            vidas--;
            CambiarEstado();
        }

        public void AgarrarPowD()
        {
            powd = true;
            CambiarEstado();
        }
        public void AgarrarPowA()
        {
            powa = true;
            aviones2 = true;
            CambiarEstado();
        }

        public void ChocarAvion()
        {
            if (!aviones2)
            {
                powa = false;
            }
            aviones2 = false;
            
            CambiarEstado();
        }

        void CambiarEstado()
        {
            if (vidas > 0)
            {
                vivo = true;
                if (!powa && !powd)
                {
                    this.estado = Estado.normal;
                }
                else if (powd)
                {
                    if (!powa)
                    {
                        estado = Estado.disparos_4;
                    }
                    else if (aviones2)
                    {
                        estado = Estado.disparos_4_Aviones_2;
                    }
                    else
                    {
                        estado = Estado.disparos_4_Aviones_1;
                    }

                }
                else if (powa)
                {
                    if(aviones2)
                    {
                        estado = Estado.disparos_2_Aviones_2;
                    }
                    else
                    {
                        estado = Estado.disparos_2_Aviones_1;
                    }

                }
            }
            else
            {
                estado = Estado.FIN;
            }
        }

    }
}