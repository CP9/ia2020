﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1942___Maquina_Estado
{
    public partial class Form1 : Form
    {

        Avion avion = new Avion();
        public Form1()
        {
            InitializeComponent();

            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = avion.Estado.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            avion.AgarrarPowD();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            avion.AgarrarPowA();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            avion.Chocar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            avion.ChocarAvion();
        }
    }
}
